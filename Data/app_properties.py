from team3_scorer import get_team3_score


JAVA_server_port = '9001'
JAVA_server_ip = 'http://catndog.northeurope.cloudapp.azure.com'
JAVA_server_url = JAVA_server_ip+':'+JAVA_server_port+'/graphql'
JAVA_server_score_url = JAVA_server_ip+':'+JAVA_server_port+'/scores/challenge3'
team_score = int(get_team3_score())
thread_flags = dict(match_maker_flag=False, confusion_matrix_thread_flag=False)
port = 5003

