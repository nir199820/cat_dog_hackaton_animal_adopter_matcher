from Data.app_properties import thread_flags
from subprocess import call
from time import sleep


def confusion_matrix_runner():
    while thread_flags['confusion_matrix_thread_flag']:
        print('########## call for matrix ##########')
        call(['python', 'Data/get_confusion_matrix_from_server.py'])
        sleep(60)
