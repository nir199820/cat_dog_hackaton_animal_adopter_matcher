import requests
from requests.exceptions import ConnectionError
import pickle
import codecs
from Data.app_properties import JAVA_server_ip, JAVA_server_port

def get_confusion_matrix():
    try:
        print("from server")
        confusion_matrix_string = requests.get(JAVA_server_ip+':'+JAVA_server_port+'/scores/getMatrix').text
        confusion_matrix_bytes = codecs.decode(confusion_matrix_string.encode(), 'base64')
        confusion_matrix = pickle.loads(confusion_matrix_bytes)
        with open("Data/confusion_matrix.pickle", 'wb') as f:
            pickle.dump(confusion_matrix, f)
    except ConnectionError:
        print("from file")
        with open("Data/confusion_matrix.pickle", 'rb') as f:
            confusion_matrix = pickle.load(f)
    return confusion_matrix


confusion_matrix = get_confusion_matrix()
print(confusion_matrix)
