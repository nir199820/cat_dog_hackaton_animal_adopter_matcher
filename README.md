# Introduction 
This is the third (and last) service of the cat & dog project.

This service is in charge of matching between the list of pets (Aka, Animals or Adoptees) to the list of people (Aka Adopters).
This service creates "matches" i.e a pair of a Pet and a Person.

After the service creates a match it sends it via GraphQL query to a remote server that handles the requests.
# Getting Started
1.	Installation process:
    You can get the source code from the remote repository in gitlab
2.	Software dependencies:
    to install all the packages needed for the project run the following command:
    
    $ pip install -r requirements.txt
3.	Latest releases
4.	API references

# Build and Test
TODO: Describe and show how to build your code and run the tests. 

# Contribute
TODO: Explain how other users and developers can contribute to make your code better. 

If you want to learn more about creating good readme files then refer the following [guidelines](https://docs.microsoft.com/en-us/azure/devops/repos/git/create-a-readme?view=azure-devops). You can also seek inspiration from the below readme files:
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)