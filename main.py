from requests_handler import match_adopters
from Data.app_properties import thread_flags, port, team_score, JAVA_server_score_url
import Data
from flask import Flask, request
import threading
import pickle
from requests import get
from Data.confusion_matrix_thread_runner import confusion_matrix_runner
from Data.get_confusion_matrix_from_server import confusion_matrix

match_maker = threading.Thread(target=match_adopters, daemon=True)
confusion_matrix_thread = threading.Thread(target=confusion_matrix_runner, daemon=True)

threads = dict(match_maker=match_maker, confusion_matrix_thread=confusion_matrix_thread)


app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def hello():
    if request.method == 'GET':
        return "Hello _GET"
    elif request.method == 'POST':
        return "Hello _POST"
    else:
        return None


@app.route('/update_confusion_matrix', methods=['POST', 'GET'])
def update_confusion_matrix():
    if request.method == 'POST':
        confusion_matrix = pickle.loads(request.data)
        print(confusion_matrix)
        with open("Data/confusion_matrix.pickle", 'wb') as f:
            pickle.dump(confusion_matrix, f)
    return "Send a POST request to this endpoint to update the confusion matrix"


@app.route('/get_status', methods=['POST', 'GET'])
def get_status():
    if thread_flags['match_maker_flag']:
        return str(team_score)
    else:
        return str(200 + team_score)


@app.route('/match_animals', methods=['POST', 'GET'])
def match_animals():
#   if request.method == 'POST':
    thread = threads['match_maker']
    if thread.is_alive():
        thread_flags['match_maker_flag'] = False
        print("finished to match adoptees to adopters")
        thread.join()
    else:
        threads['match_maker'] = match_maker
        thread_flags['match_maker_flag'] = True
        print("Beginning to match adoptees to adopters")
        thread.start()
    return "matched adopters!"
#   return "In order to match the animals send a POST request to this endpoint"


def confusion_matrix_thread_activator():
    thread = threads['confusion_matrix_thread']
    if thread.is_alive():
        thread_flags['confusion_matrix_thread_flag'] = False
        print("finished getting confusion matrix")
        thread.join()
    else:
        threads['confusion_matrix_thread'] = confusion_matrix_thread
        thread_flags['confusion_matrix_thread_flag'] = True
        print("Getting confusion matrix")
        thread.start()
    return "Got confusion matrix!"


if __name__ == '__main__':
    confusion_matrix_thread_activator()
    get(JAVA_server_score_url, params={'grade': team_score})
    match_animals()
    print("starting app")
    app.run("0.0.0.0", port)
    print("closing app")
    confusion_matrix_thread_activator()
    match_animals()

