from graphql_handler.graphqlHandler import GraphQLRequests, GraphQlMutation
from generations.conversions import from_entity_parser
from Data.app_properties import JAVA_server_url, thread_flags
from team3_solution.team3_solution import send_adopters
from Data.get_confusion_matrix_from_server import confusion_matrix
from time import sleep
from datetime import datetime


def match_adopters():
    print('called', thread_flags['match_maker_flag'])
    query = GraphQLRequests(JAVA_server_url)
    mutate = GraphQlMutation(JAVA_server_url)
    while thread_flags['match_maker_flag']:
        adopters = from_entity_parser.adopters_dictionary(query.import_adopters())
        print(datetime.now(), adopters)
        adoptees = {}
        for adoptee in query.import_adoptees(2):
            adoptees[(adoptee.x, adoptee.y)] = adoptee.pet_type.description
        print(datetime.now(), adoptees)
        matched_adoptees = send_adopters(adopters, adoptees, confusion_matrix)
        print(datetime.now(), matched_adoptees)
        for match in list(matched_adoptees.items()):
            if not thread_flags['match_maker_flag']:
                return
            if match is not None:
                mutate.adopt(match[0], match[1][0], match[1][1])