from team3_solution.team3_solution import send_adopters
import pandas as pd

team3_score = 0
TOTAL_TESTS = 9


def get_team3_score():
    return team3_score/TOTAL_TESTS * 100

########################################################
###############     SCENARIO 1   #######################
########################################################
def create_perfect_confusion_matrix():
    """
    helper function the will generate a confusion matrix of a perfect model - i.e. a model that always right!
    """
    perfect_conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0,100,0,0,0],
                                "cat": [0,0,100,0,0], "rabbit": [0,0,0,100,0],
                                "parrot": [0,0,0,0,100]})
    perfect_conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return perfect_conf_matrix

def create_scenario1():
    adopters_requests = {"adopter1": ["dog", "rabbit"], "adopter2": ["cat", "parrot"]}
    model_predictions = {(56,23): "parrot", (14,77): "rabbit"}
    confusion_mat = create_perfect_confusion_matrix()
    
    return adopters_requests, model_predictions, confusion_mat


optimal_solution_scenario_1 = {"adopter1": (14,77), "adopter2": (56,23)}
dss_solution_scenario_1 = send_adopters(*create_scenario1())

if(optimal_solution_scenario_1 == dss_solution_scenario_1):
    team3_score += 1

########################################################
###############     SCENARIO 2   #######################
########################################################
def create_scenario2():
    adopters_requests = {"adopter2": ["cat", "rabbit"], "adopter1": ["rabbit"]}
    model_predictions = {(3,92): "cat", (7, 7): "dog"}
    confusion_mat = create_perfect_confusion_matrix()
    return adopters_requests, model_predictions, confusion_mat

optimal_solution_scenario_2 = {"adopter2": (3,92)}
dss_solution_scenario_2 = send_adopters(*create_scenario2())

if(optimal_solution_scenario_2 == dss_solution_scenario_2):
    team3_score += 1

########################################################
###############     SCENARIO 3   #######################
########################################################
def create_scenario3():
    adopters_requests = {"adopter1": ["parrot", "rabbit"], "adopter2": ["rabbit", "cat"]}
    model_predictions = {(17,1): "rabbit", (46,43): "parrot"}
    confusion_mat = create_perfect_confusion_matrix()
    return adopters_requests, model_predictions, confusion_mat

# obviously for the above scenario, the optimal decision should be to send adopter1 to (50,50) and adopter2 to
# (40,40) and making 2 perfect matches that way

optimal_solution_scenario_3 = {"adopter1": (46,43), "adopter2": (17,1)}
dss_solution_scenario_3 = send_adopters(*create_scenario3())

if(optimal_solution_scenario_3 == dss_solution_scenario_3):
    team3_score += 1

########################################################
###############     SCENARIO 4   #######################
########################################################
def create_confusion_matrix_for_scenario4():
    """
    helper function that will create a confusion matrix for scenario4. 
    the confusion matrix is such that the model it represents has 100% precision for all of it's
    predictions except for dogs in which he good as a random guess.
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0,100,0,0,0],
                                "cat": [0,0,100,0,0], "rabbit": [0,25,25,25,25],
                                "parrot": [0,0,0,0,100]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario4():
    adopters_requests = {"adopter1": ["dog"], "adopter2": ["rabbit"]}
    model_predictions = {(40,40): "dog", (50,50): "rabbit"}
    confusion_mat = create_confusion_matrix_for_scenario4()
    return adopters_requests, model_predictions, confusion_mat

optimal_solution_scenario_4 = {"adopter1": (40,40)}
dss_solution_scenario_4 = send_adopters(*create_scenario4())

if(optimal_solution_scenario_4 == dss_solution_scenario_4):
    team3_score += 1

########################################################
###############     SCENARIO 5   #######################
########################################################
def create_confusion_matrix_for_scenario5():
    """
    helper function that will create a confusion matrix for scenario5. 
    the confusion matrix is such that the model it represents has 100% precision for all of the animals
    except for dog and cat: when the model predict dog, it is 50% chance that there is indeed a dog and 50% chance
    that there is a cat. when the model predict cat, it is 80% chance that threre is indeed a cat and 20% chance for None
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [3,97,0,0,0],
                                "cat": [0,10,90,0,0], "rabbit": [0,0,0,100,0],
                                "parrot": [0,0,0,0,100]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario5():
    adopters_requests = {"adopter1": ["dog", "cat"]}
    model_predictions = {(30,32): "cat", (50,57): "dog"}
    confusion_mat = create_confusion_matrix_for_scenario5()
    return adopters_requests, model_predictions, confusion_mat

# in scenario 5, when the model predicts a dog it is 50% chance that there is a dog and 50% chance there is a cat, so
# it is 100% that there is a cat or a dog, this rectangle (50,57) has 100% chance to match adopter 1.
# however when the model predicts a cat there is a 80% chance for cat and 20% chance for None, so sending the adopter to
# rectangle (30,32) has only 80% chance to make a match - thus, the optimal decision in this scenario is to send adopter1
# to rectangle (50,57)

optimal_solution_scenario_5 = {"adopter1": (30,32)}
dss_solution_scenario_5 = send_adopters(*create_scenario5())

if(optimal_solution_scenario_5 == dss_solution_scenario_5):
    team3_score += 1
    
########################################################
###############     SCENARIO 6   #######################
########################################################
def create_confusion_matrix_for_scenario6():
    """
    helper function that will create a confusion matrix for scenario6. 
    the confusion matrix is such that the model it represents has 100% precision for all of the animals
    except for dog: when the model predict dog, it is 88% chance that there is indeed a dog and 12% chance
    that there is something else which is not a dog. 
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0,100,0,0,0],
                                "cat": [12.2, 0, 87.8,0,0], "rabbit": [0,0,0,100,0],
                                "parrot": [0,0,0,0,100]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario6():
    adopters_requests = {"adopter1": ["cat"]}
    model_predictions = {(19,98): "cat"}
    confusion_mat = create_confusion_matrix_for_scenario6()
    return adopters_requests, model_predictions, confusion_mat

# in scenario 6, when the model predicts a dog it is 88% chance that there is indeed a dog in the rectangle.
# now, we can compute for adopter1 the expected_score if it will be sent to the rectangle. it is:
# p_dog*1 - (1-p_dog)*7 (where p_dog in the probability that indeed there is a dog in the rectangle).
# that is because according to our score function, we receive 1 point for good match, but loose 7 points if we send an
# adopter to a rectangle that doesn't match it. so the expected score will be 8*p_dog-1, because p_dog=0.88 , the expected
# score will be positive - so it is recommend to send the drone instead of not sending it at all, and hence receiving 0 points.

optimal_solution_scenario_6 = {"adopter1": (19,98)}
dss_solution_scenario_6 = send_adopters(*create_scenario6())

if(optimal_solution_scenario_6 == dss_solution_scenario_6):
    team3_score += 1

########################################################
###############     SCENARIO 7   #######################
########################################################
def create_confusion_matrix_for_scenario7():
    """
    helper function that will create a confusion matrix for scenario7. 
    the confusion matrix is such that the model it represents has 100% precision for all of the animals
    except for dog: when the model predict dog, it is 87% chance that there is indeed a dog and 13% chance
    that there is something else which is not a dog. 
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0, 100 ,0 ,0, 0],
                                "cat": [0,0,100,0,0], "rabbit": [0,0,0,100,0],
                                "parrot": [12.8, 0,0,0, 87.2]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario7():
    adopters_requests = {"adopter1": ["parrot"]}
    model_predictions = {(13,45): "parrot"}
    confusion_mat = create_confusion_matrix_for_scenario7()
    return adopters_requests, model_predictions, confusion_mat

# in scenario 7, when the model predicts a dog it is 87% chance that there is indeed a dog in the rectangle.
# now, we can compute for adopter1 the expected_score if it will be sent to the rectangle. it is:
# p_dog*1 - (1-p_dog)*7 (where p_dog in the probability that indeed there is a dog in the rectangle).
# that is because according to our score function, we receive 1 point for good match, but loose 7 points if we send an
# adopter to a rectangle that doesn't match it. so the expected score will be 8*p_dog-1, because p_dog=0.87 , the expected
# score will be negative - so it is recommend not to send the drone at all, thus not loosing points.

optimal_solution_scenario_7 = {}
dss_solution_scenario_7 = send_adopters(*create_scenario7())

if(optimal_solution_scenario_7 == dss_solution_scenario_7):
    team3_score += 1
    
########################################################
###############     SCENARIO 8   #######################
########################################################
def create_confusion_matrix_for_scenario8():
    """
    helper function that will create a confusion matrix for scenario8. 
    the confusion matrix is such that the model it represents has 100% precision for all of the animals
    except for dog: when the model predict dog, it is 90% chance that there is indeed a dog and 10% chance
    that there is a cat.
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0,100,0,0,0],
                                "cat": [0,0,92,8,0], "rabbit": [0,0,0,100,0],
                                "parrot": [0,0,0,0,100]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario8():
    adopters_requests = {"adopter1": ["cat"]}
    model_predictions = {(77,23): "cat"}
    confusion_mat = create_confusion_matrix_for_scenario8()
    return adopters_requests, model_predictions, confusion_mat

# in scenario 8, when the model predicts a dog it is 90% chance that there is indeed a dog in the rectangle, and 10% chance
# there is a cat.
# if we will send adopter1 to the rectangle immadiately we will receive an expected score of 0.9*1 - 0.1*7 = 0.2 
# (that's because there is 7 points penalty for mismatch).
# however if we will wait one second. with probability 0.3*1/12=0.025 an adopter with requirements will appear, and this 
# adopter has 100% chance for good match (because according to the confusion matrix, with 100% chance there is a cat or
# a dog in the rectangle). with 0.925 chance we will keep the original matching this receiving 0.2 points, so the 
# expected score in that case will be 0.025*1 + 0.975*0.2 = 0.22. because we delay our response we loose 1/3600 from the score
# but even with that we can see that the expected score with 1 second delay is bigger that the expected score if we send the
# adopter right now, so the optimal decision in that case is to wait and not sending the adopter right now.
optimal_solution_scenario_8 = {}
dss_solution_scenario_8 = send_adopters(*create_scenario8())

if(optimal_solution_scenario_8 == dss_solution_scenario_8):
    team3_score += 1
    
########################################################
###############     SCENARIO 9  ########################
########################################################
def create_confusion_matrix_for_scenario9():
    """
    helper function that will create a confusion matrix for scenario9. 
    the confusion matrix is such that the model it represents has 100% precision for all of the animals
    except for dog: when the model predict dog, it is 1-1/3600 chance that there is indeed a dog and 1/3600 chance
    that there is a cat.
    """
    conf_matrix = pd.DataFrame({"None": [100,0,0,0,0], "dog": [0,100,0,0,0],
                                "cat": [0,0,100,0,0], "rabbit": [0,0,0,7599,1],
                                "parrot": [0,0,0,0,100]})
    conf_matrix.index = ["None", "dog", "cat", "rabbit", "parrot"]
    return conf_matrix

def create_scenario9():
    adopters_requests = {"adopter5": ["rabbit"]}
    model_predictions = {(14,13): "rabbit"}
    confusion_mat = create_confusion_matrix_for_scenario9()
    return adopters_requests, model_predictions, confusion_mat

# in scenario 9 it is obviously better to send the adopter 1, because our expected score will be 1-1/3600
# we couldn't achieve better score if we wait because even if we will have 100% chance for matching we will have
# a penalty of 1/3600 to the score because of the delay thus making the expected score cannot exceed 1-1/3600
optimal_solution_scenario_9 = {"adopter5": (14, 13)}
dss_solution_scenario_9 = send_adopters(*create_scenario9())

if(optimal_solution_scenario_9 == dss_solution_scenario_9):
    team3_score += 1

